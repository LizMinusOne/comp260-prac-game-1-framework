﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public BeeMove beePrefab;
    //public PlayerMove player;
    public int nBees = 50;
    public Rect spawnRect;
    public float minBeePeriod = 40;
    public float maxBeePeriod = 60;
    private float beePeriod;
    private float time;

    void Start()
    {
        // instantiate the bees
        //BeeMove bee = Instantiate(beePrefab);
        // set the target
        //bee.target = player.transform;

        // create bees
        /*for (int i = 0; i < nBees; i++)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);

            // attach to this object in the hierarchy
            bee.transform.parent = transform;

            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin + 
                Random.value * spawnRect.width;
            float y = spawnRect.yMin + 
                Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);
        }*/

    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work
            //Vector2 v = child.position - centre;

            // Fixed bug by adding a type conversion
            Vector2 v = (Vector2)child.position - centre;

            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }
    void setRandomTime()
    {
        beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
    }

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
    // Update is called once per frame
    void Update () {

        time += Time.deltaTime;
        if(time >= beePeriod)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);

            // attach to this object in the hierarchy
            bee.transform.parent = transform;

            // give the bee a name and number
            bee.gameObject.name = "Bee";

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
                Random.value * spawnRect.width;
            float y = spawnRect.yMin +
                Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);
        }
    }
}
