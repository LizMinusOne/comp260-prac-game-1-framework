﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Move : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    public float maxSpeed = 5.0f;
    // Update is called once per frame
    void Update () {
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");
        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;
        // move the object
        transform.Translate(velocity * Time.deltaTime);
    }
}
