﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    // private state
    private float speed;
    private float turnSpeed;
    public ParticleSystem explosionPrefab;

    // Use this for initialization
    void Start () {
        // find a player object to be the target by type
        PlayerMove player = (PlayerMove)
        FindObjectOfType(typeof(PlayerMove));
        target = player.transform;
    }

    /*public float speed = 4.0f; // metres per second
    public float turnSpeed = 180.0f; // degrees per second*/
    public Transform target;
    public Transform target2;
    private Vector2 heading = Vector2.right;

    // Update is called once per frame
    void Update () {
        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;
        
        if(direction.magnitude > direction2.magnitude) {
            // calculate how much to turn per frame
            float angle = turnSpeed * Time.deltaTime;

            // turn left or right
            if (direction2.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
        }

        else {
            // calculate how much to turn per frame
            float angle = turnSpeed * Time.deltaTime;

            // turn left or right
            if (direction.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);
    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
        Gizmos.DrawRay(transform.position, direction2);
    }
}
